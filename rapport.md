---
title: "Projet Web Serveur"
author: "Quatrefages Augustin, Fortias Mathéo"
---
# PROJET TECHNOLOGIES DU WEB - CÔTÉ SERVEUR
## Quatrefages Augustin & Fortias Mathéo

### Qu'allons-nous devoir faire ?

#### **1.  Quelles sont les fonctionnalités qui vous semblent importantes ?**

Voici la liste des différentes fonctionnalités que l'on juge importantes :
- **gérer les inscriptions :** Un utilisateur doit pouvoir s'inscrire s'il ne possède pas déjà de compte.
- **gérer les connexions :** Un utilisateur doit pouvoir se connecter afin de récupérer son compte et il faut étiqueter la date de la connexion.
- **gérer les déconnexions :** Un utilisateur doit pouvoir se déconnecter de son compte.
- **génération aléatoire d'un pokemon à un utilisateur :** Chaque jour, chaque utilisateur reçoit un pokemon aléatoire (qu'il possède déjà ou non).
- **voir la liste des pokemons possédés par l'utilisateur :** Il faut pouvoir voir la liste des pokemons posséder par un utilisateur.
- **gérer les échanges de pokemons entre deux utilisateurs :** Deux utilisateurs doivent pouvoir s'échanger des pokémon de façon simple (un pokemon contre un pokemon).
- **augmenter le niveau de 5 pokemons choisis chaque jour :** Chaque jour, chaque utilisateur peut améliorer 5 pokemons.

#### **2. Quelles vont être les principales classes de votre application ?**

![UML](images/classes.png)

#### **3. Quelles vont être les tables de votre base de données ? Vous pouvez produire un diagramme UML pour mettre au clair les jointures entre les tables, mais c'est facultatif.**

![UML](images/uml.png)

#### **4. Quelles solutions technologiques envisagez vous ?**

- utilisation de la bibliotèque de java spark
- utilisation de freeMarker
- utilisation de l'API de pokemon
- utilisation de SQL
- utilisation de HTML et CSS

### Les URIs :

- GET /index $\rightarrow$ permet d'accéder à la page d'acceuil du site.

- GET /users $\rightarrow$ pour récupérer la liste de tous les utilisateurs.

- Inscription :
  - GET /inscription $\rightarrow$ recuperer la page d'inscription.
  - POST /inscription $\rightarrow$ s'inscrire (envoyer les données du formulaire).
  - GET /confirmation_inscription $\rightarrow$ récupérer la page de confirmation d'inscription.

- Connexion :
  - GET /connexion $\rightarrow$ recuperer la page de connexion.
  - POST /connexion $\rightarrow$ se connecter (envoyer les données du formulaire).
  - GET /confirmation_connexion $\rightarrow$ récupérer la page de confirmation de connexion.
  - GET /deconnexion $\rightarrow$ pour se deconnecter.

- GET /profil $\rightarrow$ récupérer la page du profil de l'utilisateur connecté.

- Obtention d'un Pokemon :
  - GET /obtention $\rightarrow$ pour obtenir un pokemon aléatoire.
  - GET /obtentionpossible $\rightarrow$ récupérer la page de confirmation d'obtention.

- Amélioration d'un Pokemon :
  - GET /amelioraton $\rightarrow$ pour obtenir la page d'amelioration.
  - POST /amelioraton $\rightarrow$ pour envoyer les données du formulaire pour demander une amelioration.
  - GET /amelioratonsuccess $\rightarrow$ pour récuperer la page de confirmation d'amelioration.

- Echange d'un Pokemon :
  - GET /echange $\rightarrow$ pour obtenir la page d'echange.
  - POST /echange/demande $\rightarrow$ pour envoyer les données du formulaire pour demander un echange.
  - POST /echange/acceptation $\rightarrow$ pour envoyer les données du formulaire pour accepter un echange.
  - GET /demandeechangesuccess $\rightarrow$ pour récuperer la page de confirmation de demande d'echange.
  - GET /acceptationechangesuccess $\rightarrow$ pour récuperer la page de confirmation d'acceptation d'echange.

### La structure du site :

##### La page d'acceuil :

La page d'acceuil de notre site (/index) sert de sommaire à notre site. On peut accéder à divers pages :
- Liste des utilisateurs $\rightarrow$ Pas besoin d'être connecté pour accéder à cette page, elle affiche simplement la liste de tous les utilisateurs. Il est possible de revenir à la page d'acceuil.
- Amelioration $\rightarrow$ Renvoie vers la page d'amelioration.
- Echange $\rightarrow$ Renvoie vers la page de connexion si aucun n'utilisateur n'est connecté, sinon renvoie vers la page d'echange.
- S'inscrire : Renvoie vers la page du formulaire d'inscription.
- Se connecter : Renvoie vers la page du formulaire de connexion.
- Profil $\rightarrow$ Renvoie vers la page de connexion si aucun n'utilisateur n'est connecté, sinon renvoie vers la page du profil de l'utilisateur connecté.

##### La page d'amelioration :

Cette page est composée tout d'abord d'un formulaire pour entrer l'id d'un Pokemon à ameliorer. Si aucun n'utilisateur n'est connecté, alors l'appuie sur le bouton Submit entraîne une redirection vers la page de connexion. Sionon, si une erreur 400 est renvoyé, c'est que le nombre d'amelioration maximale, soit 5, effectué par l'utilisateur en 24h a été atteint, ou que l'id du Pokemon n'existe pas, ou que le Pokemon esst déjà au niveau 100. Sinon, si toutes les conditions sont respectées, alors le pokemon est amélioré et l'utilisateur est redirigé vers la page de confirmation d'amelioration.

##### La page d'echange :

Cette page page est composée de 4 parties : 
- la demande d'echange : l'utilisateur doit saisir l'id de l'un de ses Pokemon qu'il souhaite echangé et l'idAPI du pokemon qu'il souhaite en echange. Une erreur 400 est renvoyée si l'id du Pokemon est inconnue ou s'il n'appartient pas à l'utilisateur, ou si l'idAPI du Pokemon souhaité n'existe pas. Sinon, l'echange est entré dans la base de données et l'utilisateur est redirigé vers la page de confirmation de demande d'echange.
- l'acceptation d'echange : l'utilisateur doit saisir l'id de l'échange qu'il souhaite accepter. Une erreur 400 est renvoyée si l'id de l'echange n'existe pas, ou si cet echange a été proposé par l'utilisateur lui-même, ou si l'utilisateur ne possède pas le Pokemon d'idAPI voulu par l'autre utilisateur qui a demandé l'echange. Sinon, la base de données est mise à jour l'utilisateur est redirigé vers la page de confirmation de'acceptation d'echange.
- la liste des Pokemons de l'utilisateur : cette partie est utile pour que l'utilisateur puisse connaître les pokemons qu'il possède et ainsi demander des echanges en fonction.
- la liste des demandes d'echange en cours : cette partie est utile pour que l'utilisateur puisse connaître toutes les demande d'echange en cours. Une demande d'echange en cours est une demande d'echange qui a été effectuée mais qui n'a pas encore été acceptée.

##### La page d'inscription :

Cette page est uniquement composée d'un formulaire qui permet à l'utilisateur de s'inscrire. L'envoie du formulaire renvoie une erreur 400 si l'un des champs du formulaire est vide, ou si le pseudo existe déjà dans la base de données. Sinon, l'utilisateur est créer dans la base de données et il est redirigé vers la page de confirmation d'inscription.

##### La page de connexion :

Cette page est uniquement composée d'un formulaire qui permet à l'utilisateur de se connecter. L'envoie du formulaire renvoie une erreur 400 si l'un des champs du formulaire est vide, ou si le couple (pseudo, password) n'existe pas dans la base de données. Sinon, on créé une session avec pour identifiant l'instance utilisateur et il est redirigé vers la page de confirmation de connexion.

##### La page du profil :

Cette page affiche simplement les données de l'utilisateur actuellement connecté. Depuis cette page, l'utilisateur peut obtenir son Pokemon quotidien en cliquant sur Obtenir mon pokemon. Ceci renverra une erreur 400 si l'utilisateur a déjà obtenu un Pokemon dans les dernieres 24h, sinon un nouveau Pokemon aléatoire est créer dans la base de données pour ect utilisateur, et il est redirgé vers la page de confirmation d'obtention qui l'informe sur le Pokemon qu'il vient d'obtenir. Il est également possible de se déconnecter en cliquant sur Se déconnecter. L'instance utilisateur est alors supprimé de la session et l'utilisateur est redirgé vers la page d'acceuil.

### Conclusion :

Ce projet de Web Serveur nous a permis d'apprendre et de comprendre la gestion d'un site Web côté serveur, et donc plus particulierement la gestion de la base de données mais aussi la correspondance entre la base de données et la partie objet. Nous avons trouvé cela très intérréssant.