<#ftl encoding="utf-8">

<body xmlns="http://www.w3.org/1999/html">
    <h1>Connexion réussie !</h1>
    <p>Vous pouvez maintenant revenir à la <a href="/index">page d'acceuil</a> et utiliser l'application.</p>
</body>
</html>