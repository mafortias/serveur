<#ftl encoding="utf-8">

<body xmlns="http://www.w3.org/1999/html">
    <h1>Profil de ${user.pseudo}</h1>
    <p>Email : ${user.email}</p>
    <h2>Liste des Pokemons</h2>
    <ul>
        <#list pokemonList as pokemon>
            <li>${pokemon.getIdPokemon()} - ${pokemon.getNomPokemon()} (idAPI : ${pokemon.getIdAPI()}) de niveau ${pokemon.getNiveau()} (${pokemon.getGenre()}, ${pokemon.getType1()}, ${pokemon.getType2()}, ${pokemon.getDescription()})</li>
        </#list>
    </ul>
    <a href="/obtention">Obtenir mon Pokemon quotidien</a>
    <a href="/deconnexion">Se déconnecter</a>
    <a href="/index">Page d'acceuil</a>
</body>
</html>