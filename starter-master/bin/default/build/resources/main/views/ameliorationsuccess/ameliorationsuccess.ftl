<#ftl encoding="utf-8">

<body xmlns="http://www.w3.org/1999/html">
    <h1>Amélioration réussie</h1>
    <p>
        Le Pokemon ${pokemon.getNomPokemon()} (id : ${pokemon.getIdPokemon()}) vient de passer au niveau ${pokemon.getNiveau()}.
    </p>
    <a href="/amelioration">Améliorer un autre Pokemon</a>
</body>
</html>