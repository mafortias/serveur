<#ftl encoding="utf-8">

<body xmlns="http://www.w3.org/1999/html">
    <h1>Amélioration</h1>
    <form action="/amelioration" method="post">
            <label for="idPokemon">Id du Pokemon à améliorer :</label><br>
            <input type="text"
                    id="idPokemon"
                    name="idPokemon"><br>
            <input type="submit" value="Améliorer">
    </form>
    <h1>Liste des Pokemons</h1>
    <ul>
        <#list pokemonList as pokemon>
            <li>${pokemon.getIdPokemon()} - ${pokemon.getNomPokemon()} de niveau ${pokemon.getNiveau()} appartenant à ${pokemon.getUtilisateur().getPseudo()}</li>
        </#list>
    </ul>
    <a href="/index">Page d'acceuil</a>
</body>
</html>