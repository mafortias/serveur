<#ftl encoding="utf-8">

<body xmlns="http://www.w3.org/1999/html">
       <h1>Inscription</h1>
       <form action="/inscription"
              method="post">
              <label for="pseudo">Pseudo :</label><br>
              <input type="text"
                     id="pseudo"
                     name="pseudo"><br>

              <label for="email">Email :</label><br>
              <input type="email"
                     id="email"
                     name="email"><br>

              <label for="password">Mot de passe :</label><br>
              <input type="password"
                     id="password"
                     name="password"><br>

              <input type="submit"
                     value="S'inscrire">
       </form>
       <a href="/index">Page d'acceuil</a>
</body>

</html>