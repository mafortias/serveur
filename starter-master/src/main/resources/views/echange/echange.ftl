<#ftl encoding="utf-8">

<body xmlns="http://www.w3.org/1999/html">
    <h1>Echanges</h1>
    <h2>Demande d'échange</h2>
    <form action="/echange/demande" method="post">
        <label for="idPokemon">Id de votre Pokemon :</label><br>
        <input type="text" id="idPokemon" name="idPokemon"><br>
        <label for="idAPIvoulu">IdAPI du Pokemon que vous souhaitez :</label><br>
        <input type="idAPIvoulu" id="idAPIvoulu" name="idAPIvoulu"><br>
        <input type="submit" value="Demander">
    </form>
    <h2>Acceptation d'échange</h2>
    <form action="/echange/acceptation" method="post">
        <label for="idEchange">Id de l'échange que vous souhaitez accepter :</label><br>
        <input type="text" id="idEchange" name="idEchange"><br>
        <input type="submit" value="Accepter">
    </form>
    <h2>Vos Pokemons :</h2>
    <ul>
        <#list pokemonList as pokemon>
            <li>${pokemon.getIdPokemon()} - ${pokemon.getNomPokemon()} (idAPI : ${pokemon.getIdAPI()}) de niveau ${pokemon.getNiveau()} (${pokemon.getGenre()}, ${pokemon.getType1()}, ${pokemon.getType2()}, ${pokemon.getDescription()})</li>
        </#list>
    </ul>
    <h2>Demandes d'échange en cours :</h2>
    <ul>
        <#list echangeList as echange>
            <li>${echange.getId()} - ${echange.getUser().getPseudo()} souhaite échanger son ${echange.getPokemon().getNomPokemon()} contre un Pokemon d'idAPI : ${echange.getIdAPIvoulu()}</li>
        </#list>
    </ul>
    <a href="/index">Page d'acceuil</a>
</body>
</html>