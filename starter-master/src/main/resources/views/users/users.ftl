<#ftl encoding="utf-8">

<body xmlns="http://www.w3.org/1999/html">

<ul>
    <#list users as user>
        <li>${user.id} - ${user.pseudo} ${user.email}</li>
    </#list>
</ul>

<a href="/index">Page d'acceuil</a>

</body>

</html>
