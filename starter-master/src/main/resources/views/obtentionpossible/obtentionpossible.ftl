<#ftl encoding="utf-8">

<body xmlns="http://www.w3.org/1999/html">

<h1>Obtention réussie</h1>
<p>Voici le pokemon que vous venez d'obtenir :</p>
<p>Nom : ${pokemon.getNomPokemon()} (idAPI : ${pokemon.getIdAPI()})</p>
<p>Niveau : ${pokemon.getNiveau()}</p>
<p>Type 1 : ${pokemon.getType1()}</p>
<p>Type 2 : ${pokemon.getType2()}</p>
<p>Genre : ${pokemon.getGenre()}</p>
<p>Description : ${pokemon.getDescription()}</p>
<p>Veuillez revenir sur votre <a href="/profil">profil</a></p>
</body>

</html>
