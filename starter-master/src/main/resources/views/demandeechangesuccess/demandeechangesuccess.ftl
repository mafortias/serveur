<#ftl encoding="utf-8">

<body xmlns="http://www.w3.org/1999/html">
    <h1>Demande d'échange réussie</h1>
    <p>
        Votre demande d'echange a bien été prise en compte, l'echange sera éffectué lorsqu'un autre utilisateur l'aura accepté.
    </p>
    <a href="/index">Acceuil</a>
</body>
</html>