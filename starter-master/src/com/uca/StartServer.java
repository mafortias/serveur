package com.uca;

import com.uca.dao._Initializer;
import com.uca.entity.*;
import com.uca.gui.*;
import com.uca.core.*;

import java.util.Random;

import static spark.Spark.*;

public class StartServer {

    public static void main(String[] args) {
        // Configure Spark
        staticFiles.location("/static/");
        port(8081);

        // Initialisation de la base de données
        _Initializer.Init();

        // redirection vers la page index
        get("/", (request, response) -> {
            response.redirect("/index");
            return null;
        });

        // Page index
        get("/index", (request, response) -> {
            return IndexGUI.getView();
        });

        // Liste des utilisateurs
        get("/users", (request, response) -> {
            // generation du contenu de la page web sous forme de String
            return UserGUI.getAllUsers();
        });

        // Page du formulaire d'inscription
        get("/inscription", (request, response) -> {
            return InscriptionGUI.getView();
        });

        // Traitement du formulaire d'inscription
        post("/inscription", (request, response) -> {
            InscriptionGUI inscription = new InscriptionGUI();
            // verif des données saisie et inscription si possible
            return inscription.tryInscription(request, response);
        });

        // Page de confirmation d'inscription
        get("/confirmation_inscription", (request, response) -> {
            return InscriptionGUI.getConfirmationView();
        });

        // Page du formulaire de connexion
        get("/connexion", (request, response) -> {
            return ConnexionGUI.getView();
        });

        // Traitement du formulaire de connexion
        post("/connexion", (request, response) -> {
            ConnexionGUI connexion = new ConnexionGUI();
            // verif des données saisie et connexion si possible
            return connexion.tryConnexion(request, response);
        });

        // Page de confirmation de connexion
        get("/confirmation_connexion", (request, response) -> {
            return ConnexionGUI.getConfirmationView();
        });

        // Page du profil
        get("/profil", (request, response) -> {
            return ProfilGUI.getView(request, response);
        });

        // Pour se déconnecter
        get("/deconnexion", (request, response) -> {
            // ecrasement de la session de connexion
            request.session().removeAttribute("user");
            response.redirect("/index"); // redirection vers la page index
            return null;
        });

        // obtention de pokemon
        get("/obtention", (request, response) -> {
            UserEntity utilisateur = request.session().attribute("user");
            if (utilisateur.obtentionPossible()) {
                // generation aleatoire d'un idAPI + creation de ce pokemon dans la table
                // listUserPokemon + redirection vers la page "vous avez obtenu ce pokemon"
                UserCore.updateDerniereObtention(utilisateur); // mise à jour de la date de derniere obtention
                Random rand = new Random();
                int idAPI = rand.nextInt(1009);
                // creation d'un pokemon de niveau 1
                ListeUtilisateurPokemonCore.createListeUtilisateurPokemon(utilisateur, idAPI, 1); 
                response.redirect("/obtentionpossible");
            } else {
                response.status(400);
                return "Vous avez déjà obtenu un Pokemon aujourd'hui !";
            }
            return null;
        });

        // obtention de pokemon effectuee
        get("/obtentionpossible", (request, response) -> {
            // on regarde le dernier pokemon obtenu par utilisateur et
            // on affiche "Vous avez obtenu ..."
            UserEntity utilisateur = request.session().attribute("user");
            return ListeUtilisateurPokemonGUI.getDonneesDernierPokemonObtenu(utilisateur);
        });

        // Page amelioration
        get("/amelioration", (request, response) -> {
            // generation du contenu de la page web sous forme de String
            return ListeUtilisateurPokemonGUI.getAllPokemon();
        });

        // Traitement du formulaire d'amelioration
        post("/amelioration", (request, response) -> {
            UserEntity utilisateur = request.session().attribute("user");
            if (utilisateur == null) { // si l'utilisateur n'est pas connecté
                response.redirect("/connexion");
                // Rediriger vers la page de connexion
                return null;
            }
            String idPokemon = request.queryParams("idPokemon"); // recup de l'idPokemon voulu
            if (idPokemon == null || idPokemon.isEmpty()) {
                response.status(400); // Bad Request
                return "Le champ est vide, il doit être renseigné";
            }
            int codeAmelioration = utilisateur.ameliorationPossible(Integer.parseInt(idPokemon));
            PokemonEntity pokemon = ListeUtilisateurPokemonCore
                    .getPokemonById(Integer.parseInt(idPokemon));
            if (codeAmelioration == 1) { // si nbMax d'amelioration atteint
                response.status(400); // Bad Request
                return "Nombre maximale d'améliorations éffectuées en 24h atteint";
            } else if (codeAmelioration == 2) { // si idPokemon n'existe pas
                response.status(400); // Bad Request
                return "L'id du pokemon est inconnu"; // si pokemon deja au niveau 100
            } else if (codeAmelioration == 3) {
                response.status(400);
                return "Le Pokemon est déjà au niveau 100";
            } else { // si amelioration possible
                // mise à jour des deux tables dans la base de données
                AmeliorationCore.setAmelioration(utilisateur.getId(), pokemon.getIdPokemon());
                ListeUtilisateurPokemonCore.setAmelioration(pokemon);
                // redirection vers la page amelioration confirmee du pokemon ...
                response.redirect("/ameliorationsuccess");
            }
            return null;
        });

        // Page de succés d'amélioration
        get("/ameliorationsuccess", (request, response) -> {
            UserEntity utilisateur = request.session().attribute("user");
            return AmeliorationGUI.getViewLastAmeliorationOfUser(utilisateur);
        });

        get("/echange", (request, response) -> {
            UserEntity utilisateur = request.session().attribute("user");
            if (utilisateur == null) { // si l'utilisateur n'est pas connecté
                response.redirect("/connexion");
                // Rediriger vers la page de connexion si l'utilisateur n'est pas connecté
                return null;
            }
            return EchangeGUI.getView(utilisateur);
        });

        post("/echange/demande", (request, response) -> {
            UserEntity utilisateur = request.session().attribute("user");
            if (utilisateur == null) { // si l'utilisateur n'est pas connecté
                response.redirect("/connexion");
                // Rediriger vers la page de connexion si l'utilisateur n'est pas connecté
                return null;
            }

            String idPokemon = request.queryParams("idPokemon");
            if (idPokemon == null || idPokemon.isEmpty()) { // si champ idPokemon vide
                response.status(400); // Bad Request
                return "Veuillez renseigner l'intégralité des champs";
            }

            String idAPIvoulu = request.queryParams("idAPIvoulu");
            if (idAPIvoulu == null || idAPIvoulu.isEmpty()) { // si champ idAPI vide
                response.status(400); // Bad Request
                return "Veuillez renseigner l'intégralité des champs";
            }

            // recup du pokemon 
            PokemonEntity pokemon = ListeUtilisateurPokemonCore.getPokemonById(Integer.parseInt(idPokemon));
            if (pokemon == null) {
                response.status(400); // Bad Request
                return "Ce Pokemon n'existe pas ou ne vous appartient pas";
            }
                
            EchangeEntity echange = new EchangeEntity(utilisateur, ListeUtilisateurPokemonCore.getPokemonById(Integer.parseInt(idPokemon)),
                    Integer.parseInt(idAPIvoulu));
            int codeDemandeEchange = echange.demandeEchangePossible();
            if (codeDemandeEchange == 1) { // si l'idPokemon choisi n'appartient pas à l'utilisateur
                response.status(400); // Bad Request
                return "Ce Pokemon n'existe pas ou ne vous appartient pas";
            } else if (codeDemandeEchange == 2) { // si l'idAPI du pokemon voulu n'existe pas
                response.status(400); // Bad Request
                return "L'idAPI du Pokemon n'existe pas";
            } else { // si la demande d'echange est possible
                EchangeCore.createEchange(echange); // creation de l'echange dans la base de données
                // redirection vers la page demande d'echange confirmee du pokemon
                response.redirect("/demandeechangesuccess");
            }
            return null;
        });

        post("/echange/acceptation", (request, response) -> {
            UserEntity utilisateur = request.session().attribute("user");
            if (utilisateur == null) {
                response.redirect("/connexion");
                // Rediriger vers la page de connexion si l'utilisateur n'est pas connecté
                return null;
            }

            String idEchange = request.queryParams("idEchange");
            if (idEchange == null || idEchange.isEmpty()) {
                response.status(400); // Bad Request
                return "Le champ est vide, il doit être renseigné";
            }

            EchangeEntity echange = EchangeCore.getEchangeById(Integer.parseInt(idEchange));
            int codeAcceptationEchange = echange.acceptationEchangePossible(utilisateur);
            if (codeAcceptationEchange == 1) { // si l'id d'echange n'existe pas
                response.status(400); // Bad Request
                return "Cet identifiant d'échange n'existe pas";
            } else if (codeAcceptationEchange == 2) {
                // si l'user tente d'echanger un pokemon avec lui-même
                response.status(400); // Bad Request
                return "Vous ne pouvez pas effectuer un échange avec vous même";
            } else if (codeAcceptationEchange == 3) {
                // si l'utilisateur ne possede pas le pokemon souhaité par la personne qui a
                // proposé l'échange
                response.status(400); // Bad Request
                return "Vous ne possédez pas le pokemon voulu par l'autre utilisateur";
            } else { // sinon
                // mise à jour de la base de données (changement de propriétaire du premier pokemon)
                ListeUtilisateurPokemonCore.nouveauPossesseur(echange.getPokemon().getIdPokemon(), utilisateur.getId());
                // recuperation du deuxième pokemon à echanger
                PokemonEntity pokemonAEchanger = ListeUtilisateurPokemonCore.getPremOccByIdAPI(utilisateur.getId(),
                        echange.getIdAPIvoulu());
                // mise à jour de la base de données (changement de propriétaire du deuxième pokemon)
                ListeUtilisateurPokemonCore.nouveauPossesseur(pokemonAEchanger.getIdPokemon(),
                        echange.getUser().getId());
                // suppression de l'echange dans la base de donnée
                EchangeCore.deleteEchange(echange);
                // redirection vers la page acceptation d'echange confirmee du pokemon
                response.redirect("/acceptationechangesuccess");
            }
            return null;
        });

        // Page de succés de demande d'echange
        get("/demandeechangesuccess", (request, response) -> {
            UserEntity utilisateur = request.session().attribute("user");
            return EchangeGUI.getConfirmationView();
        });

        // Page de succés de demande d'echange
        get("/acceptationechangesuccess", (request, response) -> {
            UserEntity utilisateur = request.session().attribute("user");
            return EchangeGUI.getConfirmationAcceptationView();
        });
    }
}
