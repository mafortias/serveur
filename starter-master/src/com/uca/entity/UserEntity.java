package com.uca.entity;

import java.sql.Timestamp;

import com.uca.core.*;

import java.sql.Date;

public class UserEntity {
    private int id;
    private String pseudo;
    private String email;
    private String password;
    private long derniereObtention;

    public UserEntity() {
        // Ignored !
    }

    public UserEntity(String pseudo, String email, String password) {
        this.pseudo = pseudo;
        this.email = email;
        this.password = password;
    }

    public UserEntity(int id, String pseudo, String email, String password, long derniereObtention) {
        this.id = id;
        this.pseudo = pseudo;
        this.email = email;
        this.password = password;
        this.derniereObtention = derniereObtention;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getDateDerniereObtention() {
        return this.derniereObtention;
    }

    public boolean possede(int idAPI) {
        return ListeUtilisateurPokemonCore.possede(this.id, idAPI);
    }

    /* Renvoie true si le la dernière obtention effectuée par
    l'utilisateur a eu lieu il y a plus de 24h. */
    public boolean obtentionPossible() {
        long nbMillisEnUnJour = 1000 * 60 * 60 * 24; // obtention possible une fois par jour
        long millis = System.currentTimeMillis();
        if ((millis - this.derniereObtention) > nbMillisEnUnJour) {
            this.derniereObtention = millis;
            return true;
        } else {
            return false;
        }
    }

    public int ameliorationPossible(int idPokemon) {
        int code;
        AmeliorationCore.deleteAncienneAmelioration();
        if (AmeliorationCore.getNbAmeliorationAjdByUserId(this.id) >= 5) {
            // si nbMax d'amelioration atteint
            code = 1;
        } else if (!ListeUtilisateurPokemonCore.isExist(idPokemon)) {
            // si idPokemon n'existe pas
            code = 2;
        } else if (ListeUtilisateurPokemonCore.getNiveauById(idPokemon) >= 100) {
            // si pokemon deja au niveau 100
            code = 3;
        } else { // sinon
            code = 4;
        }

        return code;
    }
}
