package com.uca.entity;

import com.uca.entity.*;

public class PokemonEntity {

    private UserEntity utilisateur;
    private int idPokemon;
    private int idAPI;
    private int niveau;
    private String nomPokemon;
    private String type1;
    private String type2;
    private String genre;
    private String description;

    public PokemonEntity() {

    }

    public PokemonEntity(UserEntity utilisateur, int idPokemon, int idAPI, int niveau) {
        this.utilisateur = utilisateur;
        this.idPokemon = idPokemon;
        this.idAPI = idAPI;
        this.niveau = niveau;
    }

    public PokemonEntity(int idAPI, String nomPokemon, String type1, String type2, String genre, String description) {
        this.nomPokemon = nomPokemon;
        this.type1 = type1;
        this.type2 = type2;
        this.genre = genre;
        this.description = description;
    }

    public PokemonEntity(UserEntity utilisateur, int idPokemon, int idAPI, int niveau, String nomPokemon, String type1, String type2, String genre, String description) {
        this.utilisateur = utilisateur;
        this.idPokemon = idPokemon;
        this.idAPI = idAPI;
        this.niveau = niveau;
        this.nomPokemon = nomPokemon;
        this.type1 = type1;
        this.type2 = type2;
        this.genre = genre;
        this.description = description;
    }

    public UserEntity getUtilisateur() {
        return this.utilisateur;
    }

    public void setUtilisateur(UserEntity utilisateur) {
        this.utilisateur = utilisateur;
    }

    public int getIdPokemon() {
        return idPokemon;
    }

    public void setIdPokemon(int idPokemon) {
        this.idPokemon = idPokemon;
    }

    public int getIdAPI() {
        return idAPI;
    }

    public void setIdAPI(int idAPI) {
        this.idAPI = idAPI;
    }

    public int getNiveau() {
        return niveau;
    }

    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }

    public String getNomPokemon() {
        return this.nomPokemon;
    }

    public void setNomPokemon(String nomPokemon) {
        this.nomPokemon = nomPokemon;
    }

    public String getType1() {
        return this.type1;
    }

    public void setType1(String type) {
        if (type == null) {
            this.type1 = "";
        } else {
            this.type1 = type;
        }
    }

    public String getType2() {
        return this.type2;
    }

    public void setType2(String type) {
        if (type == null) {
            this.type2 = "";
        } else {
            this.type2 = type;
        }
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}