package com.uca.entity;

import com.uca.core.*;
import com.uca.entity.PokemonEntity;

public class EchangeEntity {
    private int id;
    private UserEntity user;
    private PokemonEntity pokemon;
    private int idAPIvoulu;

    public EchangeEntity() {

    }

    public EchangeEntity(UserEntity user, PokemonEntity pokemon, int idAPIvoulu) {
        this.user = user;
        this.pokemon = pokemon;
        this.idAPIvoulu = idAPIvoulu;
    }

    public EchangeEntity(int idEchange, UserEntity user, PokemonEntity pokemon, int idAPIvoulu) {
        this.id = idEchange;
        this.user = user;
        this.pokemon = pokemon;
        this.idAPIvoulu = idAPIvoulu;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserEntity getUser() {
        return this.user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public PokemonEntity getPokemon() {
        return this.pokemon;
    }

    public void setPokemon(PokemonEntity pokemon) {
        this.pokemon = pokemon;
    }

    public int getIdAPIvoulu() {
        return idAPIvoulu;
    }

    public void setIdAPIvoulu(int idAPIvoulu) {
        this.idAPIvoulu = idAPIvoulu;
    }

    public int demandeEchangePossible() {
        int code;

        if (!ListeUtilisateurPokemonCore.appartientA(this.user.getId(), this.pokemon.getIdPokemon())) {
            // si l'idPokemon choisi n'appartient pas à l'utilisateur
            code = 1;
        } else if (this.idAPIvoulu > 1008 || this.idAPIvoulu < 1) {
            // si l'idAPI du pokemon voulu n'existe pas
            code = 2;
        } else { // sinon
            code = 3;
        }

        return code;
    }

    public int acceptationEchangePossible(UserEntity utilisateur) {
        // user : personne qui propose l'echange
        // utilisateur : personne qui accepte l'echange
        int code;
        if (!EchangeCore.isExist(this.id)) { // si l'id d'echange n'existe pas
            code = 1;
        } else if (this.user.getId() == utilisateur.getId()) {
            // si l'user tente d'echanger un pokemon avec lui-même
            code = 2;
        } else if (!utilisateur.possede(idAPIvoulu)) {
            // si l'utilisateur ne possede pas le pokemon souhaité par la personne qui a proposé l'échange
            code = 3;
        } else { // sinon
            code = 4;
        }

        return code;
    }

    @Override
    public String toString() {
        return id + " " + user.getId() + " " + pokemon.getIdPokemon() + " " + idAPIvoulu;
    }
}
