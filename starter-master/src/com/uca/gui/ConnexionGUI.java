package com.uca.gui;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import spark.Request;
import spark.Response;

import com.uca.entity.*;
import com.uca.dao.*;
import com.uca.core.UserCore;

public class ConnexionGUI {

    public static String getView() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        StringWriter writer = new StringWriter();
        Template template = configuration.getTemplate("connexion/connexion.ftl");
        template.process(null, writer);

        return writer.toString();
    }

    // Vérification des champs et creation de session si valide
    public String tryConnexion(Request request, Response response) {
        String pseudo = request.queryParams("pseudo");
        String password = request.queryParams("password");

        // Vérification des champs
        if (pseudo == null || pseudo.isEmpty()) {
            response.status(400); // Bad Request
            return "Le pseudo est obligatoire";
        }

        if (password == null || password.isEmpty()) {
            response.status(400); // Bad Request
            return "Le mot de passe est obligatoire";
        }

        // Authentification de l'utilisateur
        UserEntity utilisateur = UserCore.getUserByPseudo(pseudo);
        if (utilisateur == null) {
            response.status(400); // Bad Request
            return "Le pseudo ou le mot de passe est incorrect";
        }

        if (!utilisateur.getPassword().equals(password)) {
            response.status(400); // Bad Request
            return "Le pseudo ou le mot de passe est incorrect";
        }

        // Stockage des informations de l'utilisateur dans la session
        request.session().attribute("user", utilisateur);

        // Redirection vers une page de confirmation
        response.redirect("/confirmation_connexion");
        return null;
    }

    public static String getConfirmationView() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        StringWriter writer = new StringWriter();
        Template template = configuration.getTemplate("confirmation_connexion/confirmation_connexion.ftl");
        template.process(null, writer);

        return writer.toString();
    }
}