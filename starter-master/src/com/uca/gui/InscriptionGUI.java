package com.uca.gui;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import spark.Request;
import spark.Response;

import com.uca.entity.*;
import com.uca.core.UserCore;
import com.uca.dao.*;

public class InscriptionGUI {

    public static String getView() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        StringWriter writer = new StringWriter();
        Template template = configuration.getTemplate("inscription/inscription.ftl");
        template.process(null, writer);

        return writer.toString();
    }

    // Traitement du formulaire d'inscription et inscription si valide
    public String tryInscription(Request request, Response response) {
        String pseudo = request.queryParams("pseudo"); // doit être unique
        String email = request.queryParams("email");
        String password = request.queryParams("password");

        // Vérification des champs et insertion dans la base de données
        if (pseudo == null || pseudo.isEmpty()) {
            response.status(400); // Bad Request
            return "Le pseudo est obligatoire";
        }

        if (email == null || email.isEmpty()) {
            response.status(400); // Bad Request
            return "L'email est obligatoire";
        }

        if (password == null || password.isEmpty()) {
            response.status(400); // Bad Request
            return "Le mot de passe est obligatoire";
        }

        // Vérification de l'unicité du pseudo
        if (UserCore.isPseudoExist(pseudo)) {
            response.status(400); // Bad Request
            return "Le pseudo est déjà utilisé";
        }

        // Insertion dans la base de données
        UserEntity utilisateur = new UserEntity(pseudo, email, password);
        UserCore.createUser(utilisateur);

        // Redirection vers une page de confirmation
        response.redirect("/confirmation_inscription");
        return null;
    }

    public static String getConfirmationView() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        StringWriter writer = new StringWriter();
        Template template = configuration.getTemplate("confirmation_inscription/confirmation_inscription.ftl");
        template.process(null, writer);

        return writer.toString();
    }   
}