package com.uca.gui;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

public class IndexGUI {
    // vue de la page d'acceuil
    public static String getView() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        StringWriter writer = new StringWriter();
        Template template = configuration.getTemplate("index/index.ftl");
        template.process(null, writer);

        return writer.toString();
    }
}