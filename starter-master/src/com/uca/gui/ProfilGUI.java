package com.uca.gui;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;

import spark.Request;
import spark.Response;

import com.uca.entity.*;
import com.uca.dao.*;
import com.uca.core.*;

public class ProfilGUI {
    // vu du profil de l'utilisateur
    public static String getView(Request request, Response response) throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        UserEntity user = request.session().attribute("user");
        if (user == null) {
            response.redirect("/connexion");
            // Rediriger vers la page de connexion si l'utilisateur n'est pas connecté
            return null;
        }

        UserEntity utilisateur = UserCore.getUserByPseudo(user.getPseudo());
        ArrayList<PokemonEntity> pokemonList = ListeUtilisateurPokemonCore
                .getAllListePokemonByUserId(utilisateur.getId());

        Map<String, Object> input = new HashMap<>();
        input.put("user", utilisateur);
        input.put("pokemonList", pokemonList);

        StringWriter output = new StringWriter();
        Template template = configuration.getTemplate("profil/profil.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }
}