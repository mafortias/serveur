package com.uca.gui;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import spark.Request;
import spark.Response;

import com.uca.entity.*;
import com.uca.core.*;
import com.uca.dao.*;

public class EchangeGUI {
    public static String getView(UserEntity utilisateur) throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        // recup de la liste des pokemon detenu par l'utilisateur
        ArrayList<PokemonEntity> pokemonList = ListeUtilisateurPokemonCore
                .getAllListePokemonByUserId(utilisateur.getId());

        // recup de liste de tous les echanges
        ArrayList<EchangeEntity> echangeList = EchangeCore.getAllEchange();

        Map<String, Object> input = new HashMap<>();
        input.put("pokemonList", pokemonList);

        input.put("echangeList", echangeList);

        StringWriter writer = new StringWriter();
        Template template = configuration.getTemplate("echange/echange.ftl");
        template.process(input, writer);

        return writer.toString();
    }

    // afficher la vue de  la confirmation de demande d'echange
    public static String getConfirmationView() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        StringWriter writer = new StringWriter();
        Template template = configuration.getTemplate("demandeechangesuccess/demandeechangesuccess.ftl");
        template.process(null, writer);

        return writer.toString();
    }

    // afficher la vue de la confirmation d'acceptation d'echange
    public static String getConfirmationAcceptationView() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        StringWriter writer = new StringWriter();
        Template template = configuration.getTemplate("acceptationechangesuccess/acceptationechangesuccess.ftl");
        template.process(null, writer);

        return writer.toString();
    }
}