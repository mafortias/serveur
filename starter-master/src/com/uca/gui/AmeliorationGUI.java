package com.uca.gui;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.util.HashMap;
import java.util.Map;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import spark.Request;
import spark.Response;

import com.uca.entity.*;
import com.uca.core.*;
import com.uca.dao.*;

public class AmeliorationGUI {
    /* Pour afficher l'ameiloration qui vient d'être effectuée */
    public static String getViewLastAmeliorationOfUser(UserEntity user) throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        // recup de l'idPokemon qui vient d'être amélioré
        int idPokemon = AmeliorationCore.getIdPokemonOfLastAmeliorationByUserId(user.getId());
        // recup du pokemon qui vient d'être amélioré
        PokemonEntity pokemon = ListeUtilisateurPokemonCore.getPokemonById(idPokemon);

        Map<String, Object> input = new HashMap<>();
        input.put("pokemon", pokemon);

        StringWriter output = new StringWriter();
        Template template = configuration.getTemplate("ameliorationsuccess/ameliorationsuccess.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }
}