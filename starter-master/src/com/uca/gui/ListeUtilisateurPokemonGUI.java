package com.uca.gui;

import com.uca.core.*;
import com.uca.entity.*;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.ArrayList;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ListeUtilisateurPokemonGUI {
    // vu de la liste de tous les pokemons
    public static String getAllPokemon() throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        Map<String, Object> input = new HashMap<>();

        ArrayList<PokemonEntity> pokemonList = ListeUtilisateurPokemonCore
                .getAllListeUtilisateurPokemon();

        input.put("pokemonList", pokemonList);

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("amelioration/amelioration.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }

    // vu du dernier pokemon obteni par l'utilisateur
    public static String getDonneesDernierPokemonObtenu(UserEntity utilisateur) throws IOException, TemplateException {
        Configuration configuration = _FreeMarkerInitializer.getContext();

        // récupération des données du dernier pokemon obtenu par l'utilisateur
        PokemonEntity pokemon = ListeUtilisateurPokemonCore
                .getDonneesDernierPokemonObtenuByIdUser(utilisateur.getId());

        Map<String, Object> input = new HashMap<>();
        input.put("pokemon", pokemon);

        Writer output = new StringWriter();
        Template template = configuration.getTemplate("obtentionpossible/obtentionpossible.ftl");
        template.setOutputEncoding("UTF-8");
        template.process(input, output);

        return output.toString();
    }
}