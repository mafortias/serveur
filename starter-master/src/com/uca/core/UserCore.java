package com.uca.core;

import com.uca.dao.UserDAO;
import com.uca.entity.*;

import java.util.ArrayList;

public class UserCore {

    public static ArrayList<UserEntity> getAllUsers() {
        return new UserDAO().getAllUsers();
    }

    public static boolean isPseudoExist(String pseudo) {
        return new UserDAO().isPseudoExist(pseudo);
    }

    public static ArrayList<UserEntity> getAllUsersByEchangeList(
            ArrayList<EchangeEntity> echangeList) {
        return new UserDAO().getAllUsersByEchangeList(echangeList);
    }

    public static UserEntity getUserByPseudo(String pseudo) {
        return new UserDAO().getUserByPseudo(pseudo);
    }

    public static UserEntity getUserById(int id) {
        return new UserDAO().getUserById(id);
    }

    public static UserEntity createUser(UserEntity user) {
        return new UserDAO().create(user);
    }

    public static void deleteUser(UserEntity user) {
        new UserDAO().delete(user);
    }

    public static void updateDerniereObtention(UserEntity user) {
        new UserDAO().updateDerniereObtention(user);
    }

    public static ArrayList<UserEntity> getAllInfoUserByList(
            ArrayList<PokemonEntity> listePokemon) {
        return new UserDAO().getAllInfoUserByList(listePokemon);
    }
}