package com.uca.core;

import com.uca.dao.*;
import com.uca.entity.*;

import java.util.ArrayList;

public class ListeUtilisateurPokemonCore {

    public static ArrayList<PokemonEntity> getAllListeUtilisateurPokemon() {
        return new ListeUtilisateurPokemonDAO().getAllListe();
    }

    public static ArrayList<PokemonEntity> getAllListePokemonByUserId(int id) {
        return new ListeUtilisateurPokemonDAO().getAllListePokemonByUserId(id);
    }

    public static PokemonEntity createListeUtilisateurPokemon(UserEntity user, int idAPI, int niveau) {
        PokemonEntity pokemon = new PokemonEntity();
        pokemon.setUtilisateur(user);
        pokemon.setIdAPI(idAPI);
        pokemon.setNiveau(niveau);
        return new ListeUtilisateurPokemonDAO().create(pokemon);
    }

    public static ArrayList<PokemonEntity> getAllListePokemonByEchangeList(
            ArrayList<EchangeEntity> echangeList) {
        return new ListeUtilisateurPokemonDAO().getAllListePokemonByEchangeList(echangeList);
    }

    public static void deleteListeUtilisateurPokemon(PokemonEntity pokemon) {
        new ListeUtilisateurPokemonDAO().delete(pokemon);
    }

    public static boolean appartientA(int idUser, int idPokemon) {
        return new ListeUtilisateurPokemonDAO().appartientA(idUser, idPokemon);
    }

    public static boolean possede(int idUser, int idAPI) {
        return new ListeUtilisateurPokemonDAO().possede(idUser, idAPI);
    }

    public static void nouveauPossesseur(int idPokemon, int idUser) {
        new ListeUtilisateurPokemonDAO().nouveauPossesseur(idPokemon, idUser);
    }

    public static PokemonEntity getPremOccByIdAPI(int idUser, int idAPI) {
        return new ListeUtilisateurPokemonDAO().getPremOccByIdAPI(idUser, idAPI);
    }

    public static PokemonEntity getDonneesDernierPokemonObtenuByIdUser(int id) {
        return new ListeUtilisateurPokemonDAO().getDonneesDernierPokemonObtenuByIdUser(id);
    }

    public static PokemonEntity getPokemonById(int id) {
        return new ListeUtilisateurPokemonDAO().getPokemonById(id);
    }

    public static boolean isExist(int idPokemon) {
        return new ListeUtilisateurPokemonDAO().isExist(idPokemon);
    }

    public static int getNiveauById(int idPokemon) {
        return new ListeUtilisateurPokemonDAO().getNiveauById(idPokemon);
    }

    public static void setAmelioration(PokemonEntity pokemon) {
        new ListeUtilisateurPokemonDAO().setAmelioration(pokemon);
    }
}