package com.uca.core;

import com.uca.dao.*;
import com.uca.entity.*;

import java.util.ArrayList;

public class AmeliorationCore {

    public static int getNbAmeliorationAjdByUserId(int id) {
        return new AmeliorationDAO().getNbAmeliorationAjdByUserId(id);
    }

    public static int getIdPokemonOfLastAmeliorationByUserId(int userId) {
        return new AmeliorationDAO().getIdPokemonOfLastAmeliorationByUserId(userId);
    }

    public static void setAmelioration(int idUser, int idPokemon) {
        new AmeliorationDAO().setAmelioration(idUser, idPokemon);
    }

    public static void deleteAncienneAmelioration() {
        new AmeliorationDAO().deleteAncienneAmelioration();
    }
}
