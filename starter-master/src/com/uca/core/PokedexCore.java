package com.uca.core;

import com.uca.dao.*;
import com.uca.entity.*;

import java.util.ArrayList;

public class PokedexCore {
    public static PokemonEntity getInfoPokemonByIdAPI(int idAPI) {
        return new PokedexDAO().getInfoPokemonByIdAPI(idAPI);
    }
}
