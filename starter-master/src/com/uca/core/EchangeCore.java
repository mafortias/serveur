package com.uca.core;

import com.uca.dao.*;
import com.uca.entity.*;

import java.util.ArrayList;

public class EchangeCore {

    public static ArrayList<EchangeEntity> getAllEchange() {
        return new EchangeDAO().getAllEchange();
    }

    public static boolean isExist(int idEchange) {
        return new EchangeDAO().isExist(idEchange);
    }

    public static EchangeEntity getEchangeById(int idEchange) {
        return new EchangeDAO().getEchangeById(idEchange);
    }

    public static EchangeEntity createEchange(EchangeEntity echange) {
        return new EchangeDAO().create(echange);
    }

    public static void deleteEchange(EchangeEntity echange) {
        new EchangeDAO().delete(echange);
    }
}
