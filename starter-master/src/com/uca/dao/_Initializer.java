package com.uca.dao;

import java.sql.*;

import java.util.Random;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileNotFoundException;
import java.io.IOException;

public class _Initializer {

    public static void Init() {
        Connection connection = _Connector.getInstance();

        try {
            PreparedStatement statement;

            // Init des différentes tables de la DB
            // echange
            // statement = connection.prepareStatement("DROP TABLE echange; ");
            // statement.executeUpdate();
            // ameliorations
            // statement = connection.prepareStatement("DROP TABLE ameliorations; ");
            // statement.executeUpdate();
            // listUserPokemon
            // statement = connection.prepareStatement("DROP TABLE listUserPokemon; ");
            // statement.executeUpdate();
            // users
            // statement = connection.prepareStatement("DROP TABLE users; ");
            // statement.executeUpdate();
            
            // users
            statement = connection.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS users (id int primary key auto_increment, pseudo varchar(100), email varchar(200), password varchar(100), derniereObtention bigint);");
            statement.executeUpdate();
            // listUserPokemon
            statement = connection.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS listUserPokemon (id int primary key auto_increment, idUser int, FOREIGN KEY(idUser) REFERENCES users(id), idAPI int, niveau int);");
            statement.executeUpdate();
            // ameliorations
            statement = connection.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS ameliorations (idAmelioration int primary key auto_increment, idUser int, FOREIGN KEY(idUser) REFERENCES users(id), idPokemon int, FOREIGN KEY(idPokemon) REFERENCES listUserPokemon(id), date bigint);");
            statement.executeUpdate();
            // echange
            statement = connection.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS echange (idEchange int primary key auto_increment, idUser int, FOREIGN KEY(idUser) REFERENCES users(id), idPokemon int, FOREIGN KEY(idPokemon) REFERENCES listUserPokemon(id), idAPIvoulu int);");
            statement.executeUpdate();
            
        } catch (Exception e) {
            // e.printStackTrace();
            throw new RuntimeException("could not create database !");
        }
    }
}