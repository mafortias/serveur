package com.uca.dao;

import com.uca.entity.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.Random;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class PokedexDAO {
    /* Recupère les infos du pokemon ayant cet idAPI dans l'API PokeAPI */
    public PokemonEntity getInfoPokemonByIdAPI(int idAPI) {
        PokemonEntity info_pokemon = new PokemonEntity();
        JSONParser jsonP = new JSONParser();
        try {
            // lecture des données JSON
            URL url = new URL("https://pokeapi.co/api/v2/pokemon/" + idAPI);
            // System.out.println(url.toString());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();

            int responsecode = conn.getResponseCode();
            if (responsecode != 200) {
                throw new RuntimeException("HttpResponseCode: " + responsecode);
            } else {
                String inline = "";
                Scanner scanner = new Scanner(url.openStream());
                while (scanner.hasNext()) {
                    inline += scanner.nextLine();
                }
                scanner.close();
                JSONParser parse = new JSONParser();
                JSONObject data_obj = (JSONObject) parse.parse(inline);

                // name
                String nomPokemon = (String) data_obj.get("name");
                // type1 et type 2
                JSONArray types = (JSONArray) data_obj.get("types");
                String type1 = "";
                String type2 = "";
                if (types.size() >= 1) {
                    JSONObject type1dico = (JSONObject) types.get(0);
                    JSONObject new_obj1 = (JSONObject) type1dico.get("type");
                    type1 = (String) new_obj1.get("name");
                }
                if (types.size() == 2) {
                    JSONObject type2dico = (JSONObject) types.get(1);
                    JSONObject new_obj2 = (JSONObject) type2dico.get("type");
                    type2 = (String) new_obj2.get("name");
                }
                // genre
                Random rand = new Random();
                int int_random = rand.nextInt(2);
                String genre;
                if (int_random == 0) {
                    genre = "male";
                } else {
                    genre = "femelle";
                }
                // description
                String description = "";
                URL url_species = new URL("https://pokeapi.co/api/v2/pokemon-species/" + idAPI);
                HttpURLConnection conn_species = (HttpURLConnection) url_species.openConnection();
                conn_species.setRequestMethod("GET");
                conn_species.connect();

                int responsecode_species = conn_species.getResponseCode();
                if (responsecode_species != 200) {
                    throw new RuntimeException("HttpResponseCode: " + responsecode_species);
                } else {
                    String inline_species = "";
                    Scanner scanner_species = new Scanner(url_species.openStream());
                    while (scanner_species.hasNext()) {
                        inline_species += scanner_species.nextLine();
                    }
                    scanner_species.close();
                    JSONParser parse_species = new JSONParser();
                    JSONObject data_obj_species = (JSONObject) parse_species.parse(inline_species);
                    JSONArray flavors = (JSONArray) data_obj_species.get("flavor_text_entries");
                    for (int i = 0; i < flavors.size(); i++) {
                        JSONObject flavor = (JSONObject) flavors.get(i);
                        JSONObject language_object = (JSONObject) flavor.get("language");
                        String language = (String) language_object.get("name");
                        if (language.equals("fr")) {
                            description = (String) flavor.get("flavor_text");
                            break;
                        }
                    }
                }
                // initialisation des attributs de l'instance
                info_pokemon.setIdAPI(idAPI);
                info_pokemon.setNomPokemon(nomPokemon);
                info_pokemon.setType1(type1);
                info_pokemon.setType2(type2);
                info_pokemon.setGenre(genre);
                info_pokemon.setDescription(description);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return info_pokemon;
    }


}
