package com.uca.dao;

import com.uca.entity.*;

import java.sql.*;
import java.util.ArrayList;

import java.sql.Date;

public class AmeliorationDAO {

    public Connection connect = _Connector.getInstance();

    /* Renvoie le nombre d'ameliorations effectuées par
    l'utilisateur d'identifiant idUser dans les dernières
    24h */
    public int getNbAmeliorationAjdByUserId(int idUser) {
        long nbMillisEnUnJour = 1000 * 60 * 60 * 24; // obtention possible 5 fois par jour
        int nbAmeliorationAjd = 0;
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement("SELECT * FROM ameliorations WHERE idUser = ? and ? - date < ?;");
            preparedStatement.setInt(1, idUser);
            preparedStatement.setLong(2, System.currentTimeMillis());
            preparedStatement.setLong(3, nbMillisEnUnJour);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                nbAmeliorationAjd += 1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nbAmeliorationAjd;
    }

    /* Renvoie l'identifiant du dernier pokemon améliorer par l'utilisateur */
    public int getIdPokemonOfLastAmeliorationByUserId(int userId) {
        int idPokemon = 0;
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement("SELECT idPokemon FROM ameliorations WHERE date = (SELECT MAX(date) FROM ameliorations WHERE idUser = ?);");
            preparedStatement.setInt(1, userId);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                idPokemon = rs.getInt("idPokemon");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return idPokemon;
    }

    /* Creation de l'amélioration dans la base de données */
    public void setAmelioration(int idUser, int idPokemon) {
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement("INSERT INTO ameliorations (idUser, idPokemon, date) VALUES(?,?,?)",
                            Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, idUser);
            preparedStatement.setInt(2, idPokemon);
            preparedStatement.setLong(3, System.currentTimeMillis());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /* Suppression de toutes les améliorations qui
    ne se sont pas faite dans les dernières 24h */
    public void deleteAncienneAmelioration() {
        long nbMillisEnUnJour = 1000 * 60 * 60 * 24; // obtention possible 5 fois par jour
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement("DELETE FROM ameliorations WHERE ? - date > ?");
            preparedStatement.setLong(1, System.currentTimeMillis());
            preparedStatement.setLong(2, nbMillisEnUnJour);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
