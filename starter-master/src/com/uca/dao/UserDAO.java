package com.uca.dao;

import com.uca.entity.*;

import java.sql.*;
import java.util.ArrayList;

import java.sql.Date;

public class UserDAO extends _Generic<UserEntity> {
    /* Renvoie laliste de tous les utilisateurs */
    public ArrayList<UserEntity> getAllUsers() {
        ArrayList<UserEntity> entities = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM users ORDER BY id ASC;");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                UserEntity entity = new UserEntity();
                entity.setId(resultSet.getInt("id"));
                entity.setPseudo(resultSet.getString("pseudo"));
                entity.setEmail(resultSet.getString("email"));

                entities.add(entity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return entities;
    }

    /* Renvoie true s'il existe un utilisateur avec ce pseudo
    dans la base de données */
    public boolean isPseudoExist(String pseudo) {
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement("SELECT * FROM users WHERE pseudo = ? ;");
            preparedStatement.setString(1, pseudo);
            ResultSet rs = preparedStatement.executeQuery();
            return rs.next(); // il y a plus d'une ligne de sortie
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /* Renvoie l'utlisateur qui possède ce pseudo */
    public UserEntity getUserByPseudo(String pseudo) {
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement("SELECT * FROM users WHERE pseudo = ? ;");
            preparedStatement.setString(1, pseudo);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) { // si il existe un utilisateur avec ce pseudo
                int id = rs.getInt("id");
                String email = rs.getString("email");
                String password = rs.getString("password");
                long derniereObtention = rs.getLong("derniereObtention");
                return new UserEntity(id, pseudo, email, password, derniereObtention);
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /* Renvoie l'utilisateur qui possède cet id */
    public UserEntity getUserById(int id) {
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement("SELECT * FROM users WHERE id = ? ;");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) { // si il existe un utilisateur avec cet id
                String pseudo = rs.getString("pseudo");
                String email = rs.getString("email");
                String password = rs.getString("password");
                long derniereObtention = rs.getLong("derniereObtention");
                return new UserEntity(id, pseudo, email, password, derniereObtention);
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /* Modifie la date de dernière obtention de l'utilisateur */
    public void updateDerniereObtention(UserEntity obj) {
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement(
                    "UPDATE users SET derniereObtention=? WHERE id=?;");
            preparedStatement.setLong(1, System.currentTimeMillis());
            preparedStatement.setInt(2, obj.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    /* Renvoie la liste de tous les utilisateurs apparaissant dans la liste des pokemons */
    public ArrayList<UserEntity> getAllInfoUserByList(ArrayList<PokemonEntity> listePokemon) {
        ArrayList<UserEntity> listeInfoUser = new ArrayList<>();
        for (int i = 0; i < listePokemon.size(); i++) {
            listeInfoUser.add(getUserById(listePokemon.get(i).getUtilisateur().getId()));
        }
        return listeInfoUser;
    }

    /* Renvoie la liste de tous les utilisateurs apparaissant dans la liste des echanges */
    public ArrayList<UserEntity> getAllUsersByEchangeList(
            ArrayList<EchangeEntity> echangeList) {
        ArrayList<UserEntity> liste = new ArrayList<>();
        for (EchangeEntity echange : echangeList) {
            try {
                PreparedStatement preparedStatement = this.connect
                        .prepareStatement("SELECT id, pseudo, email, password FROM users WHERE id = ?;");
                preparedStatement.setInt(1, echange.getUser().getId());
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    UserEntity elem = new UserEntity();
                    elem.setId(resultSet.getInt("id"));
                    elem.setPseudo(resultSet.getString("pseudo"));
                    elem.setEmail(resultSet.getString("email"));
                    elem.setPassword(resultSet.getString("password"));

                    liste.add(elem);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return liste;
    }

    @Override
    public UserEntity create(UserEntity obj) {
        // TODO !
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement(
                    "INSERT INTO users(pseudo, email, password, derniereObtention) VALUES(?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, obj.getPseudo());
            preparedStatement.setString(2, obj.getEmail());
            preparedStatement.setString(3, obj.getPassword());
            preparedStatement.setLong(4, 0);
            preparedStatement.executeUpdate();

            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                obj.setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return obj;
    }

    
    @Override
    public void delete(UserEntity obj) {
        // TODO !
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("DELETE FROM users WHERE id = ?");
            preparedStatement.setInt(1, obj.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
