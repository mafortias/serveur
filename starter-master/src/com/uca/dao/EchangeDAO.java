package com.uca.dao;

import com.uca.entity.*;

import com.uca.core.*;

import java.sql.*;
import java.util.ArrayList;

public class EchangeDAO extends _Generic<EchangeEntity> {
    
    /* Renvoie la liste de tous les echanges */
    public ArrayList<EchangeEntity> getAllEchange() {
        ArrayList<EchangeEntity> entities = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("SELECT * FROM echange ORDER BY idEchange ASC;");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                EchangeEntity entity = new EchangeEntity();
                entity.setId(resultSet.getInt("idEchange"));
                entity.setUser(UserCore.getUserById(resultSet.getInt("idUser")));
                entity.setPokemon(ListeUtilisateurPokemonCore.getPokemonById(resultSet.getInt("idPokemon")));
                entity.setIdAPIvoulu(resultSet.getInt("idAPIvoulu"));
                entities.add(entity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return entities;
    }

    /* Renvoie true si l'echange existe dans la base de données */
    public boolean isExist(int idEchange) {
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement("SELECT * FROM echange WHERE idEchange = ? ;");
            preparedStatement.setInt(1, idEchange);
            ResultSet rs = preparedStatement.executeQuery();
            return rs.next(); // il y a plus d'une ligne de sortie
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /* Renvoie une instance d'echange en fonction de l'idEchange */
    public EchangeEntity getEchangeById(int idEchange) {
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement("SELECT * FROM echange WHERE idEchange = ? ;");
            preparedStatement.setInt(1, idEchange);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) { // si il existe un utilisateur avec ce pseudo
                UserEntity user = UserCore.getUserById(rs.getInt("idUser"));
                PokemonEntity pokemon = ListeUtilisateurPokemonCore.getPokemonById(rs.getInt("idPokemon"));
                int idAPIvoulu = rs.getInt("idAPIvoulu");
                return new EchangeEntity(idEchange, user, pokemon, idAPIvoulu);
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public EchangeEntity create(EchangeEntity obj) {
        // TODO !
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement(
                    "INSERT INTO echange(idUser, idPokemon, idAPIvoulu) VALUES(?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, obj.getUser().getId());
            preparedStatement.setInt(2, obj.getPokemon().getIdPokemon());
            preparedStatement.setInt(3, obj.getIdAPIvoulu());
            preparedStatement.executeUpdate();

            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                obj.setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return obj;
    }

    @Override
    public void delete(EchangeEntity obj) {
        // TODO !
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement("DELETE FROM echange WHERE idEchange = ?");
            preparedStatement.setInt(1, obj.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
