package com.uca.dao;

import com.uca.entity.*;

import com.uca.core.*;

import java.sql.*;
import java.util.ArrayList;

public class ListeUtilisateurPokemonDAO extends _Generic<PokemonEntity> {

    /* Renvoie la liste de tous les pokemons de tous les utilisateurs */
    public ArrayList<PokemonEntity> getAllListe() {
        ArrayList<PokemonEntity> liste = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement("SELECT id, idUser, idAPI, niveau FROM listUserPokemon;");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                PokemonEntity elem = new PokemonEntity();
                int idPokemon = resultSet.getInt("id");
                UserEntity user = UserCore.getUserById(resultSet.getInt("idUser"));
                int idAPI = resultSet.getInt("idAPI");
                int niveau = resultSet.getInt("niveau");
                PokemonEntity infoPokemon = PokedexCore.getInfoPokemonByIdAPI(idAPI);
                liste.add(new PokemonEntity(user, idPokemon, idAPI, niveau, infoPokemon.getNomPokemon(),
                        infoPokemon.getType1(), infoPokemon.getType2(), infoPokemon.getGenre(),
                        infoPokemon.getDescription()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return liste;
    }

    /* Renvoie la liste de tous les pokemons presents dans la liste des echanges */
    public ArrayList<PokemonEntity> getAllListePokemonByEchangeList(
            ArrayList<EchangeEntity> echangeList) {
        ArrayList<PokemonEntity> listePokemon = new ArrayList<>();
        for (EchangeEntity echange : echangeList) {
            try {
                PreparedStatement preparedStatement = this.connect
                        .prepareStatement("SELECT id, idUser, idAPI, niveau FROM listUserPokemon WHERE id = ?;");
                preparedStatement.setInt(1, echange.getPokemon().getIdPokemon());
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    PokemonEntity elem = new PokemonEntity();
                    elem.setIdPokemon(resultSet.getInt("id"));
                    elem.setUtilisateur(UserCore.getUserById(resultSet.getInt("idUser")));
                    elem.setIdAPI(resultSet.getInt("idAPI"));
                    elem.setNiveau(resultSet.getInt("niveau"));

                    listePokemon.add(elem);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return listePokemon;        
    }

    /* Renvoie la liste de tous les Pokemons posséder par l'utilisateur */
    public ArrayList<PokemonEntity> getAllListePokemonByUserId(int userId){
        ArrayList<PokemonEntity> liste = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement("SELECT id, idUser, idAPI, niveau FROM listUserPokemon WHERE idUser = ?;");
            preparedStatement.setInt(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                PokemonEntity elem = new PokemonEntity();
                int idPokemon = resultSet.getInt("id");
                UserEntity user = UserCore.getUserById(resultSet.getInt("idUser"));
                int idAPI = resultSet.getInt("idAPI");
                int niveau = resultSet.getInt("niveau");
                PokemonEntity infoPokemon = PokedexCore.getInfoPokemonByIdAPI(idAPI);
                liste.add(new PokemonEntity(user, idPokemon, idAPI, niveau, infoPokemon.getNomPokemon(),
                        infoPokemon.getType1(), infoPokemon.getType2(), infoPokemon.getGenre(),
                        infoPokemon.getDescription()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return liste;
    }

    /* Renvoie le dernier Pokemon obtenu par l'utilisateur */
    public PokemonEntity getDonneesDernierPokemonObtenuByIdUser(int id) {
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement(
                            "SELECT id, idUser, idAPI, niveau FROM listUserPokemon WHERE idUser = ? AND id = (SELECT MAX(id) FROM listUserPokemon WHERE idUser = ?)");
            preparedStatement.setInt(1, id);
            preparedStatement.setInt(2, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                PokemonEntity elem = new PokemonEntity();
                int idPokemon = resultSet.getInt("id");
                UserEntity user = UserCore.getUserById(resultSet.getInt("idUser"));
                int idAPI = resultSet.getInt("idAPI");
                int niveau = resultSet.getInt("niveau");
                PokemonEntity infoPokemon = PokedexCore.getInfoPokemonByIdAPI(idAPI);
                return new PokemonEntity(user, idPokemon, idAPI, niveau, infoPokemon.getNomPokemon(),
                        infoPokemon.getType1(), infoPokemon.getType2(), infoPokemon.getGenre(),
                        infoPokemon.getDescription());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    /* Renvoie le Pokemon en fonction de l'id donné */
    public PokemonEntity getPokemonById(int id) {
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement("SELECT * FROM listUserPokemon WHERE id = ? ;");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) { // si il existe un pokemon avec cet id
                UserEntity user = UserCore.getUserById(rs.getInt("idUser"));
                int idAPI = rs.getInt("idAPI");
                int niveau = rs.getInt("niveau");
                PokemonEntity infoPokemon = PokedexCore.getInfoPokemonByIdAPI(idAPI);
                return new PokemonEntity(user, id, idAPI, niveau, infoPokemon.getNomPokemon(), infoPokemon.getType1(), infoPokemon.getType2(), infoPokemon.getGenre(), infoPokemon.getDescription());
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    /* Augmente le niveau du Pokemon spécifié */
    public void setAmelioration(PokemonEntity pokemon) {
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement(
                    "UPDATE listUserPokemon SET niveau=? WHERE id=?;");
            preparedStatement.setInt(1, pokemon.getNiveau() + 1);
            preparedStatement.setInt(2, pokemon.getIdPokemon());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /* Renvoie true si le Pokemon avec cet id existe dans la base de données */
    public boolean isExist(int id) {
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement("SELECT * FROM listUserPokemon WHERE id = ? ;");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            return rs.next(); // il y a plus d'une ligne de sortie
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /* Renvoie le niveau du Pokemon demandé */
    public int getNiveauById(int idPokemon) {
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement("SELECT * FROM listUserPokemon WHERE id = ? ;");
            preparedStatement.setInt(1, idPokemon);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) { // si il existe un utilisateur avec cet id
                int niveau = rs.getInt("niveau");
                return niveau;
            } else {
                return -1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /* Change l'id de l'utilisateur qui possède ce Pokemon */
    public void nouveauPossesseur(int idPokemon, int idUser) {
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement(
                    "UPDATE listUserPokemon SET idUser = ? WHERE id = ?;");
            preparedStatement.setInt(1, idUser);
            preparedStatement.setInt(2, idPokemon);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /* Renvoie le Pokemon avce cet idAPI possédé par l'utilisateur
    avec cet idUser */
    public PokemonEntity getPremOccByIdAPI(int idUser, int idAPI) {
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement("SELECT * FROM listUserPokemon WHERE idUser = ? and idAPI = ?;");
            preparedStatement.setInt(1, idUser);
            preparedStatement.setInt(2, idAPI);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) { // si il existe un utilisateur avec cet id
                int id = rs.getInt("id");
                int niveau = rs.getInt("niveau");
                return new PokemonEntity(UserCore.getUserById(idUser), id, idAPI, niveau);
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /* Renvoie true si l'utilisateur possède un Pokemon avce cet idAPI */
    public boolean possede(int idUser, int idAPI) {
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement("SELECT * FROM listUserPokemon WHERE idUser = ? and idAPI = ?;");
            preparedStatement.setInt(1, idUser);
            preparedStatement.setInt(2, idAPI);
            ResultSet rs = preparedStatement.executeQuery();
            return rs.next(); // il y a plus d'une ligne de sortie
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /* Renvoie true si le pokemon d'idPokemon appartient
    à l'utilisateur d'idUser */
    public boolean appartientA(int idUser, int idPokemon) {
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement("SELECT * FROM listUserPokemon WHERE id = ? and idUser = ?;");
            preparedStatement.setInt(1, idPokemon);
            preparedStatement.setInt(2, idUser);
            ResultSet rs = preparedStatement.executeQuery();
            return rs.next(); // il y a plus d'une ligne de sortie
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public PokemonEntity create(PokemonEntity obj) {
        try {
            PreparedStatement preparedStatement = this.connect.prepareStatement(
                    "INSERT INTO listUserPokemon(idUser, idAPI, niveau) VALUES(?,?,?);");
            preparedStatement.setInt(1, obj.getUtilisateur().getId());
            preparedStatement.setInt(2, obj.getIdAPI());
            preparedStatement.setInt(3, obj.getNiveau());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return obj;
    }

    @Override
    public void delete(PokemonEntity obj) {
        try {
            PreparedStatement preparedStatement = this.connect
                    .prepareStatement("DELETE FROM listUserPokemon WHERE id = ?;");
            preparedStatement.setInt(1, obj.getIdPokemon());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}