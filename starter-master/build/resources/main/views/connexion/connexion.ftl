<#ftl encoding="utf-8">

<body xmlns="http://www.w3.org/1999/html">
    <h1>Connexion</h1>
    <form action="/connexion" method="post">
        <label for="pseudo">Pseudo:</label><br>
        <input type="text" id="pseudo" name="pseudo"><br>
        <label for="password">Mot de passe:</label><br>
        <input type="password" id="password" name="password"><br><br>
        <input type="submit" value="Connexion">
    </form>
    <a href="/index">Page d'acceuil</a>
</body>
</html>