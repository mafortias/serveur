<#ftl encoding="utf-8">

<body xmlns="http://www.w3.org/1999/html">
    <h1>Inscription confirmée !</h1>
    <p>Vous pouvez maintenant vous <a href="/connexion">connecter</a> avec vos identifiants.</p>
</body>
</html>